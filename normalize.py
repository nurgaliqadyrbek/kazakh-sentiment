import json
class KazAnalysis:
    lemmas=[]
    nounH={}
    nounS={}
    verbH={}
    verbS={}
    stopWord=[]

    def __init__(self):
        self.lemmas.extend(self.jsonLoader('./dictionary/lemmas.json'))
        self.nounH=self.jsonLoader('./dictionary/noun/affixHardNoun.json')
        self.verbH=self.jsonLoader('./dictionary/verb/affixHardVerb.json')
        self.nounS=self.jsonLoader('./dictionary/noun/affixSoftNoun.json')
        self.verbS=self.jsonLoader('./dictionary/verb/affixSoftVerb.json')
        #self.stopWord.append(self.stopWorList())
        with open('./dictionary/stopWords.txt', "r",encoding='utf-8') as f:
            self.stopWord = f.read().split()
    
    def jsonLoader(self,fileName):
        with open(fileName, 'r',encoding='utf-8') as f:
            distros_dict = json.load(f)
        return distros_dict
    
    def findLem(self,word):
        for i in range(1,len(self.lemmas)):
            if self.lemmas[i]['word']==word:
                return i
        return 0    
    
    def isHard(self,word):
        chars = set('аоұықғ')
        if any((c in chars) for c in word):
            return True
        return False;
    def assimilation(self,s):
        return s[:-1]+'қ' if s[-1:]=='ғ' else s[:-1]+'к' if s[-1:]=='г' else s[:-1]+'п' if s[-1:]=='б' else s
    
    def lemmatization(self,token):
        iLem=self.findLem(token)
        aff=[]
        if iLem!=0:
            return self.lemmas[iLem],aff
        affixesnoun={}
        affixesverb={}
        if self.isHard(token):
            affixesnoun=self.nounH
            affixesverb=self.verbH
        else:
            affixesnoun=self.nounS
            affixesverb=self.verbS
        
        tempToken=token
        stem=''
        #длина анализируемого слова L(w);
        lenWord=len(token)
        #максимальная длина окончания данного слова
        maxLenAff=lenWord-2
        maxLenAff=min(maxLenAff,6)       
        
        for lenAff in range(maxLenAff,0,-1):
            for afn in affixesnoun[str(lenAff)]:
                if token.endswith(afn['affixes']):
                    tempToken=self.assimilation(token[:-len(afn['affixes'])])
                    iLem=self.findLem(tempToken)
                    aff.extend(afn['more'])
                    if len(stem)==0:
                        stem=tempToken
                    if iLem!=0:
                        return self.lemmas[iLem],aff
                    else:
                         #длина анализируемого слова L(w);
                        l1=len(tempToken)
                        #максимальная длина окончания данного слова
                        maxLAff=l1-2
                        if maxLAff<1:
                            continue
                        maxLAff=min(maxLAff,6)
                        for lenVerAff in range(maxLAff,0,-1):
                            for afv in affixesverb[str(lenVerAff)]:
                                if tempToken.endswith(afv['affixes']):
                                    tempToken=self.assimilation(tempToken[:-len(afv['affixes'])])
                                    iLem=self.findLem(tempToken)
                                    aff.extend(afv['more'])
                                    if iLem!=0:
                                        return self.lemmas[iLem],aff
                        
        maxLenAff=min(maxLenAff,6)
        for lenAff in range(maxLenAff,0,-1):
            aff=[]
            for afv in affixesverb[str(lenAff)]:
                if token.endswith(afv['affixes']):
                    tempToken=self.assimilation(token[:-len(afv['affixes'])])
                    iLem=self.findLem(tempToken)
                    aff.extend(afv['more'])
                    if len(stem)==0:
                        stem=tempToken
                    if iLem!=0:
                        return self.lemmas[iLem],aff            
            
        tmp=self.lemmas[0]
        if len(stem)==0:
            stem=tempToken
        tmp['word']=stem
        return tmp,aff